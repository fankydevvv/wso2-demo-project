package com.wso2manager.jwtwso2demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@SpringBootApplication
public class JwtWso2DemoApplication {

    public static void main(String[] args) {

        SpringApplication.run(JwtWso2DemoApplication.class, args);

    }

}
