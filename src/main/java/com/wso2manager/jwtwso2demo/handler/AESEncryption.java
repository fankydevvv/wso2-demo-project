package com.wso2manager.jwtwso2demo.handler;

import com.wso2manager.jwtwso2demo.controller.HelloWorldController;

import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.IvParameterSpec;
import java.util.Arrays;

public class AESEncryption {

    private static final String AES_ALGORITHM = "AES";
    private static final int AES_KEY_SIZE = 256;

    public static void performAESEncryption() throws Exception {
        // Generate a random AES key
        KeyGenerator keyGen = KeyGenerator.getInstance(AES_ALGORITHM);
        SecureRandom secureRandom = new SecureRandom();
        keyGen.init(AES_KEY_SIZE, secureRandom);
        Key key = keyGen.generateKey();

        // Encrypt the plaintext from the HelloWorldController
        String plaintext = HelloWorldController.firstPage();
        byte[] ciphertext = encrypt(plaintext, key);

        // Decrypt the ciphertext
        String decryptedPlaintext = decrypt(ciphertext, key);

        System.out.println("Plaintext: " + plaintext);
        System.out.println("Ciphertext: " + new String(ciphertext, StandardCharsets.UTF_8));
        System.out.println("Decrypted plaintext: " + decryptedPlaintext);
    }

    private static byte[] encrypt(String plaintext, Key key) throws Exception {
        // Encryption using AES-CBC mode with PKCS5Padding
        Cipher cipher = Cipher.getInstance(AES_ALGORITHM + "/CBC/PKCS5Padding");
        byte[] iv = new byte[cipher.getBlockSize()];
        new SecureRandom().nextBytes(iv);
        cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(iv));
        byte[] ciphertext = cipher.doFinal(plaintext.getBytes(StandardCharsets.UTF_8));
        return concatenateByteArrays(iv, ciphertext);
    }

    private static String decrypt(byte[] ciphertext, Key key) throws Exception {
        // Decryption using AES-CBC mode with PKCS5Padding
        Cipher cipher = Cipher.getInstance(AES_ALGORITHM + "/CBC/PKCS5Padding");
        byte[] iv = Arrays.copyOfRange(ciphertext, 0, cipher.getBlockSize());
        byte[] data = Arrays.copyOfRange(ciphertext, cipher.getBlockSize(), ciphertext.length);
        cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv));
        byte[] plaintextBytes = cipher.doFinal(data);
        return new String(plaintextBytes, StandardCharsets.UTF_8);
    }

    private static byte[] concatenateByteArrays(byte[] a, byte[] b) {
        byte[] result = new byte[a.length + b.length];
        System.arraycopy(a, 0, result, 0, a.length);
        System.arraycopy(b, 0, result, a.length, b.length);
        return result;
    }

}
